import numpy as np

from src.function.sum import sum_my


def test_sum():
    """
    Test of function sum

    Returns
    -------

    """
    # case0
    a_arr = np.asarray([0, 1, 2])
    b_arr = np.asarray([0, 1, 2])
    result_0 = sum_my(a_arr, b_arr)
    assert result_0 == 6
